Introduction
============

This Dockerfile provides a debian based image with the following
host and cross compilers targeting Linux:

 * x86_64 (+ Windows)
 * aarch64
 * arm
 * powerpc64
 * riscv64

The prerequisite to build and use this image is a docker-ce installation.

To build the docker image do:

```console
$ cd org.eclipse.tcf.agent
$ docker build -t tcf-builder docker
```

To build the tcf agent with the above mentioned host and target compilers do:
```console
$ docker run -it --rm -v $PWD/agent:/src -u (id -u):(id -g) tcf-builder
```

To get a shell and invoke one of the compilers in the image do:
```console
$ docker run -it --rm -v $PWD/agent:/src -u (id -u):(id -g) tcf-builder bash
```
